db.fruits.insertMany([
{
name : "Apple",
color : "Red",
stock : 20,
price: 40,
supplier_id : 1,
onSale : true,
origin: [ "Philippines", "US" ]
},
{
name : "Banana",
color : "Yellow",
stock : 15,
price: 20,
supplier_id : 2,
onSale : true,
origin: [ "Philippines", "Ecuador" ]
},
{
name : "Kiwi",
color : "Green",
stock : 25,
price: 50,
supplier_id : 1,
onSale : true,
origin: [ "US", "China" ]
},
{
name : "Mango",
color : "Yellow",
stock : 10,
price: 120,
supplier_id : 2,
onSale : false,
origin: [ "Philippines", "India" ]
}
]);




/*
	AGGREGATION
	-Definition


	AGGREGATE METHODS
	1. Match
	2. Group
	3. Sort
	4. Project
	
	AGGREGATION PIPELINES

	OPERATORS
	1. Sum
	2. Max
	3. Min
	4. Avg

*/


// MongoDB Aggregation

/*
	Used to generate manipulated data and perform operations to create filtered results that helpos in analyzing data

	Compared to doing CRUD Operations on our data from our previous sessions, aggregation gives us access to manipulate, filter, and compute for results, providing us with information to make necessary development decisions


	Aggregations in MongoDB are very flexible and you can  form your own aggregation pipeline depending on the need of your application


*/


// "$match"

// Syntax  
// 	{$match: {field: value}}

db.fruits.aggregate([
	{$match: {onSale:true}}

	]);


db.fruits.aggregate([
	{$match: {stock: {$gte: 20} } }

	]);



// "$group"

// Syntax  
// 	{$group: {_id: "value", fieldResult: "valueResult"}}


db.fruits.aggregate([
{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } }
]);


// "$sort"
/*	
-Providing the value of -1 will sort the documents in a descending order
	-Providing the value of 1 will sort the documents in an ascending order
*/

// Syntax  
// 	{$sort: {field: 1/-1}}

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"} }},
		{$sort: {total: -1}}

	]);




// "$project"
	// include or exclue


// Syntax  
// 	{$sort: {field: 1/0}}



db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"} }},
		{$project: {_id: 0}}

	]);


db.fruits.aggregate([
		{$match: {onSale: true}},
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"} }},
		{$project: {_id: 0}}
		{$sort: {total: -1}}

	]);


// Aggregation Pipelines

/*	
	-Syntax
		db.collectionName.aggregate([
			{stageA},
			{stageB},
			{stageC}

	]);

*/


// OPERATORS

// $sum, gets the total of everything


db.fruits.aggregate([
		{$group: {_id: "$supplier_id", total: {$sum: "$stock"} }},

	]);


// $max, - gets highest value


db.fruits.aggregate([
		{$group: {_id: "$supplier_id", max_price: {$max: "$price"} }},

	]);

// $min, - gets lowest value


db.fruits.aggregate([
		{$group: {_id: "$supplier_id", min_price: {$min: "$price"} }},

	]);



// $avg, - gets average value


db.fruits.aggregate([
		{$group: {_id: "$supplier_id", avg_price: {$avg: "$price"} }},

	]);